<ul>
<li>
<div class="date">
{{date}}
</div>
</li>
<li>
<h2>
<a href="
{{post_link}}
">
{{title}}
</a>
</h2>
</li>
<li>
<div class="category">
{{category}}
</div>
</li>
</ul>