import os
import sys
import constants
import errors
import shutil
import json


class Project:


    class File(object):


        def createFile(self, name, extention, path):

            self.file_name = name + '.' + extention
            self.new_file = open(self.file_name, 'w')
            self.new_file.close()

        def copyFile(self, src, dist):

            shutil.copy2(src, dist)

        def writeFile(self, file_name):
            pass

        def readFile(self, file_name) -> list:

            self.values = []
            self.action = 'open'
            file_n = self.openFile(file_name,self.action)
            print(file_n)
            for fn in file_n:
                self.values.append(fn)
            return self.values
        


    class Json:

        # Get all ID in current json file

        def getId(self, json_file):

            with open(json_file, 'r') as j_file:
                data_id = json.load(j_file)
                print(data_id['id'])

        
    class JnFile:


        def getParams(self, jn_file):

           with open(jn_file, 'r') as file_jn:
                for jn in file_jn:
                    pass


    def createFolder(self, folder_name):

        os.mkdir(folder_name)

    def checkFolder(self, path_name):

        self.folder_exist = False
        if os.path.exists(path_name):
            self.folder_exist = True
        return self.folder_exist

    # Function for create project

    def createProject(self, project_name):

        self.project_folder = constants.PROJECT_PATH + project_name
        if not self.checkFolder(self.project_folder):
            self.createFolder(self.project_folder)
        else:
            print(errors.folder_exists)

        # Copy all folders and files from ./source in ./projects/{project_name}
        shutil.copytree(constants.SOURCE_PATH, self.project_folder, dirs_exist_ok=True)

    def getCommand(self, command: list):
        if command[1] == 'create_project':
            self.createProject(command[2])



myProject = Project()
myProject.getCommand(sys.argv)

#my_json = Project.Json()
#my_json.getId('json.json')

#myFile = Project.File()
#print(myFile.readFile('postID'))
